#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = "Alexis Métaireau"
SITENAME = "Code, etc."
SITEURL = ""
THEME = 'theme'

PATH = "content"
TIMEZONE = "Europe/Paris"
DEFAULT_LANG = 'fr'

LINKS = (
	('My weblog', 'https://blog.notmyidea.org/'),
	('Github', 'https://github.com/almet/'),
	('Gitlab', 'https://gitlab.com/almet/'),
	('Mastodon', 'https://tutut.delire.party/@almet'),
)

DEFAULT_PAGINATION = 10

THEME_STATIC_PATHS = ['static']
PLUGIN_PATHS = ["."]
PLUGINS = [
    "simplereader",
]

STATIC_PATHS = [
    'images',
    'images/favicon.ico',
    'extra/robots.txt',
    ]
EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'images/favicon.ico': {'path': 'favicon.ico'},
    }

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
RSS_FEED_SUMMARY_ONLY = True
