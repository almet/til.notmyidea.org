# Sharing code snippets

This is the git repository behind https://til.notmyidea.org, where I share my
code snippets. To run it locally :

```bash
python -m virtualenv .venv
.venv/bin/pip install -r requirements.txt
.venv/bin/pelican -lr
```
